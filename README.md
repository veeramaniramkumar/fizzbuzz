# fizzbuzz



Minimal [Spring Boot](http://projects.spring.io/spring-boot/) fizzbuzz app.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally
```shell
mvn clean install
```
```shell
mvn test
```

```shell
mvn spring-boot:run
```

## Accessing the Application

By default the application is deployed in port 8080

FizzBuzz API - http://localhost:8080/fizzbuzz?entry={values}

values are comma separated integers

Example request : http://localhost:8080/fizzbuzz?entry=15,42,30

## Coverage

Rest API
Supports array of inputs
endpoint will apply the fizzbuzz output with the following rules:

If the entry is a multiple of 3, the output must be "fizz"

If the entry is a multiple of 5, the output must be "buzz"

If the entry is a multiple of 3 and 5, the output must be "fizzbuzz"

Otherwise the output must be the entry

An error is thrown if the entry is not a Int

If there is no entry or it's empty, the output must be an array with all the answers from 1 to 100

Supports "if multiple of 3, 5 and 7 output "FizzBuzzBazz""

Unit & Integration Tests
