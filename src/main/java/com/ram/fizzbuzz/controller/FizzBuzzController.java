package com.ram.fizzbuzz.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ram.fizzbuzz.service.FizzBuzzService;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class FizzBuzzController {

	@NonNull
	private FizzBuzzService fizzbuzzService;

	@GetMapping(path = "/fizzbuzz", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> getFizzBuzz(@RequestParam int[] entry) {

		return new ResponseEntity<List<String>>(fizzbuzzService.getFizzBuzz(entry), HttpStatus.ACCEPTED);

	}
}
