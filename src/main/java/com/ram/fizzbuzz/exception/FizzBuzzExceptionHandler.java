package com.ram.fizzbuzz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class FizzBuzzExceptionHandler {

	@ExceptionHandler(NumberFormatException.class)
	public ResponseEntity<ErrorMessage> fizzBuzzException(NumberFormatException ex) {

		return new ResponseEntity<>(new ErrorMessage(HttpStatus.NOT_ACCEPTABLE.value(), "entry should be an integer -> "+ex.getLocalizedMessage()),
				HttpStatus.NOT_ACCEPTABLE);
	}

}
