package com.ram.fizzbuzz.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class FizzBuzzService {

	private Map<Integer, String> rules = new HashMap<Integer, String>() {
		{
			put(3, "fizz");
			put(5, "buzz");
			put(7, "bazz");
		}
	};

	public List<String> getFizzBuzz(int[] input) {
		ArrayList<String> result = new ArrayList<>();

		for (int entry : input) {

			result.add(calculateResultFromRules(entry));

		}

		return result.isEmpty() ? getDefault() : result;
	}

	private ArrayList<String> getDefault() {
		ArrayList<String> defaultResult = new ArrayList<>();
		for (int i = 1; i <= 100; i++) {
			defaultResult.add(calculateResultFromRules(i));
		}

		return defaultResult;
	}

	private String calculateResultFromRules(int input) {

		String result = rules.entrySet().stream().filter(x -> (input % x.getKey() == 0)).map(k -> k.getValue())
				.collect(Collectors.joining(""));

		return result.isEmpty() ? String.valueOf(input) : result;
	}

}