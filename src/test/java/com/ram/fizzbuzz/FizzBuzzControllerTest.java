package com.ram.fizzbuzz;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class FizzBuzzControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		this.mockMvc.perform(get("/fizzbuzz").queryParam("entry", "")).andDo(print())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().is2xxSuccessful()).andExpect(content().string(containsString(
						"[\"1\",\"2\",\"fizz\",\"4\",\"buzz\",\"fizz\",\"bazz\",\"8\",\"fizz\",\"buzz\",\"11\",\"fizz\",\"13\",\"bazz\",\"fizzbuzz\",\"16\",\"17\",\"fizz\",\"19\",\"buzz\",\"fizzbazz\",\"22\",\"23\",\"fizz\",\"buzz\",\"26\",\"fizz\",\"bazz\",\"29\",\"fizzbuzz\",\"31\",\"32\",\"fizz\",\"34\",\"buzzbazz\",\"fizz\",\"37\",\"38\",\"fizz\",\"buzz\",\"41\",\"fizzbazz\",\"43\",\"44\",\"fizzbuzz\",\"46\",\"47\",\"fizz\",\"bazz\",\"buzz\",\"fizz\",\"52\",\"53\",\"fizz\",\"buzz\",\"bazz\",\"fizz\",\"58\",\"59\",\"fizzbuzz\",\"61\",\"62\",\"fizzbazz\",\"64\",\"buzz\",\"fizz\",\"67\",\"68\",\"fizz\",\"buzzbazz\",\"71\",\"fizz\",\"73\",\"74\",\"fizzbuzz\",\"76\",\"bazz\",\"fizz\",\"79\",\"buzz\",\"fizz\",\"82\",\"83\",\"fizzbazz\",\"buzz\",\"86\",\"fizz\",\"88\",\"89\",\"fizzbuzz\",\"bazz\",\"92\",\"fizz\",\"94\",\"buzz\",\"fizz\",\"97\",\"bazz\",\"fizz\",\"buzz\"]")));
	}
	@Test
	public void shouldReturnError() throws Exception {
		this.mockMvc.perform(get("/fizzbuzz").queryParam("entry", "test")).andDo(print())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isNotAcceptable());
						}
}
