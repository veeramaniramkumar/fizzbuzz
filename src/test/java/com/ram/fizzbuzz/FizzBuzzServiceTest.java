package com.ram.fizzbuzz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.ram.fizzbuzz.service.FizzBuzzService;



public class FizzBuzzServiceTest extends FizzBuzzApplicationTests {
	
	
	FizzBuzzService fizzBuzzService;
	
	public FizzBuzzServiceTest() {
		fizzBuzzService=new FizzBuzzService();
	}
	
	
	@Test
	public void testFizzNumbers() {
		
		int[] numbers= {3};
		ArrayList<String> al = new ArrayList<>();
		al.add("fizz");
		Assertions.assertEquals(al, fizzBuzzService.getFizzBuzz(numbers));
	}
	@Test
	public void testBuzzNumbers() {
		
		int[] numbers= {5};
		ArrayList<String> al = new ArrayList<>();
		al.add("buzz");
		Assertions.assertEquals(al, fizzBuzzService.getFizzBuzz(numbers));
	}
	
	@Test
	public void testFizzBuzzNumbers() {
		
		int[] numbers= {30};
		ArrayList<String> al = new ArrayList<>();
		al.add("fizzbuzz");
		Assertions.assertEquals(al, fizzBuzzService.getFizzBuzz(numbers));
	}
	
	@Test
	public void testNonFizzBuzzNumbers() {
		
		int[] numbers= {8,2,4};
		ArrayList<String> al = new ArrayList<>();
		al.add("8");
		al.add("2");
		al.add("4");
		Assertions.assertEquals(al, fizzBuzzService.getFizzBuzz(numbers));
	}
	
	@Test
	public void testAllCombinationNumbers() {
		
		int[] numbers= {15,21,4};
		ArrayList<String> al = new ArrayList<>();
		al.add("fizzbuzz");
		al.add("fizzbazz");
		al.add("4");
		Assertions.assertEquals(al, fizzBuzzService.getFizzBuzz(numbers));
	}
	
	@Test
	public void testEmptyArray() {
		
		int[] numbers= {};
		String[] res= {"1","2","fizz","4","buzz","fizz","bazz","8","fizz","buzz","11","fizz","13","bazz","fizzbuzz","16","17","fizz","19","buzz","fizzbazz","22","23","fizz","buzz","26","fizz","bazz","29","fizzbuzz","31","32","fizz","34","buzzbazz","fizz","37","38","fizz","buzz","41","fizzbazz","43","44","fizzbuzz","46","47","fizz","bazz","buzz","fizz","52","53","fizz","buzz","bazz","fizz","58","59","fizzbuzz","61","62","fizzbazz","64","buzz","fizz","67","68","fizz","buzzbazz","71","fizz","73","74","fizzbuzz","76","bazz","fizz","79","buzz","fizz","82","83","fizzbazz","buzz","86","fizz","88","89","fizzbuzz","bazz","92","fizz","94","buzz","fizz","97","bazz","fizz","buzz"};
		List<String> list = new ArrayList<>(Arrays.asList(res));
		Assertions.assertEquals(list, fizzBuzzService.getFizzBuzz(numbers));
	}
}
